![Build Status](https://gitlab.com/lmauromb/sample-website/badges/master/build.svg)

---

Example plain HTML site using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

## Learn Enough HTML To Be Dangerous

> An introduction to HyperText Markup Language, the language of the World Wide Web. You'll learn the most important HTML tags by building a simple but real website, which you'll deploy to the live Web in the very first section!

> *by Michael Hartl*

### TODO

> This is my course progress

1. [x] Basic HTML
  - [x] HTML tags
  - [x] Starting the project
  - [x] The first tag
  - [x] An HTML skeleton
2. [ ] Filling the index page
  - [x] Headings
  - [ ] Text formatting
  - [ ] Links
  - [ ] Adding imges
3. [ ] More pages, more tags
  - [ ] An HTML page about HTML
  - [ ] Tables
  - [ ] Divs and spans
  - [ ] Lists
  - [ ]  A navigation menu
4. [ ] Inline styling
   - [ ] Text styling
   - [ ] Floats
   - [ ] Applying a margin
   - [ ] More margin tricks
   - [ ] Box styling
   - [ ] Navigation styling
5. [ ] Conclusion
